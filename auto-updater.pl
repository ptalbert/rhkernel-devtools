#!/usr/bin/env perl

# This script tries to auto-update the lab tool for our current COPR
# repository.
#
# Author: Bruno Meneguele <bmeneg@redhat.com>

use v5.20;
use strict;
use warnings;

# Use signatures, but ignore warnings about it being experimental
use feature qw(signatures);
no warnings qw(experimental::signatures);

use English;
use Getopt::Long qw(GetOptions);
use Pod::Usage qw(pod2usage);
use File::chdir;
use File::Copy qw(move);
use File::Which;

use Data::Dumper;

my $DEFAULT_CACHE_DIR = '.cache';
my $DEFAULT_LAB_REPO_PATH = "$DEFAULT_CACHE_DIR/lab";
my $DEFAULT_LAB_REPO_URL = 'github.com/zaquestion/lab.git';
my $DEFAULT_SPECS_DIR_RHEL = './specs/rhel/';
my $DEFAULT_SPECS_DIR_FEDORA = './specs/fedora/';

# Hash for storing command line argument values.
my %opts = (
	changelog => 0,
	packager => undef,
	lab_url => undef,
	lab_path => undef,
	range => undef,
	specs_dir_rhel => undef,
	specs_dir_fedora => undef,
	skip_fedora => 0,
	skip_rhel => 0,
	skip_repo_check => 0,
	help => 0,
	man => 0
);

# Hash layout to store module info.
# It isn't used directly, but to create copies with same layout.
my %go_module_info = (
	name => undef,
	url => undef,
	specfile => undef,
	version_curr => undef,
	version_prev => undef
);

exit main(@ARGV);

sub help_message {
	# Pod::Usage allows you to specify which section you want to print in
	# case that call is executed, but for that, you need to pass -verbose
	# at max level (99).
	pod2usage(
		-exitval => 0,
		-verbose => 99,
		-sections => 'SYNOPSIS'
	) if $opts{help};

	pod2usage(
		-exitval => 0,
		-verbose => 2
	) if $opts{man};
}

sub args_parse {
	GetOptions(
		'changelog' => \$opts{changelog},
		'packager=s' => \$opts{packager},
		'lab-url=s' => \$opts{lab_url},
		'lab-path=s' => \$opts{lab_path},
		'range=s' => \$opts{range},
		'specs-dir-rhel=s' => \$opts{specs_dir_rhel},
		'specs-dir-fedora=s' => \$opts{specs_dir_fedora},
		'skip-fedora' => \$opts{skip_fedora},
		'skip-rhel' => \$opts{skip_rhel},
		'skip-repo-check' => \$opts{skip_repo_check},
		'help' => \$opts{help},
		'man' => \$opts{man}
	) or pod2usage(
		-exitval => 2,
		-verbose => 0
	);

	if ($opts{help} or $opts{man}) {
		help_message();
	}
}

# run_command runs a system command in a specified directory.
# Return:
# 	@{"<output>", <return value>}
sub run_command($cmd, $cmd_dir) {
	my @cmd_out;
	{
		local $CWD = $cmd_dir;
		@cmd_out = (join('', qx($cmd)), $?);
	}
	return \@cmd_out;
}

# git_tag_diff get the last two git tag refs.
# Return:
# 	@[<latest version>, <latest-1 version>]
sub git_tag_diff($path) {
	my @versions;
	
	if ($opts{range}) {
		my ($prev, $curr) = split(/\.\./, $opts{range});
		@versions = ($curr, $prev);
	} else {
		my ($git_refs, $err) = @{ run_command('git rev-list --tags --max-count=2', $path) };
		die "failed to get git rev-list" if $err;
		$git_refs =~ tr/\n/ /;

		(my $git_tags, $err) = @{ run_command("git describe --abbrev=0 --tags $git_refs", $path) };
		die "failed to get git describe" if $err;
		@versions = split('\n', $git_tags);
	}

	return \@versions;
}

# specfile_prepare_changelog generates the changelog text to be placed in the
# specfile. If --changelog is explicitly passed by the user in the command
# call, an editor will be opened to fill the changelog text. Otherwise, a
# generic message is used.
# Return:
# 	The changelog text with a trailing \n.
sub specfile_prepare_changelog($module) {
	my $changelog_text = '';
	my $packager = $opts{packager} if $opts{packager};
	my ($date, $err) = @{ run_command('date +"%a %b %d %Y"', '.') };
	die "failed to retrieve date" if $err;
	$date =~ tr/\n//d;

	if (not $packager) {
		print "\n>> Enter the packager \"Name <email>\": ";
		$packager = <>;
		chomp $packager;
		# no.. we don't check for name+email validity
	}

	if ($opts{changelog}) {
		my $changelog_title = qq{
			# The content of this file is going to be used as a \%changelog entry
			# for the follow module specfile:
			#
			# $module->{name}
			#
			# These commented lines are going to be skipped.
			* $date $packager - $module->{version_curr}-1
			};
		$changelog_title =~ s/^\s+//mg;

		open(my $changelog_fh, '>', '.CHANGELOG');
		print $changelog_fh $changelog_title . "\n";
		close $changelog_fh;
		
		# choose an editor: $EDITOR has precedence to git core.editor
		my $editor = "vim";
		if (not defined $ENV{EDITOR}) {
			my ($git_editor, $err) = @{ run_command("git config core.editor") };
			die "failed to run git config" if $err;
			$editor = $git_editor if $git_editor;
		} else {
			$editor = $ENV{EDITOR}; 
		}

		# we don't care about the editor's output, call it directly
		system("$editor .CHANGELOG");
		
		open($changelog_fh, '<', '.CHANGELOG');
		while (<$changelog_fh>) {
			next if /^\s*#/; # skip comments
			next if /^\s*$/; # skip blank lines
			$changelog_text = $changelog_text . $_;
		}
		close $changelog_fh;
		unlink '.CHANGELOG';
	} else {
		$changelog_text = qq{
			* $date $packager - $module->{version_curr}-1
			- Rebase to upstream release $module->{version_curr}
			};	
	}
	
	$changelog_text =~ s/^\s+//mg;
	return $changelog_text . "\n";
}

# commit_full_sha get the full commit SHA from a shortened one. It uses the
# cached repos for fetching the information, so no other extra tool is
# required.
sub commit_full_sha($path, $short_sha) {
	my ($out, $err) = @{ run_command("git rev-list -1 $short_sha 2>&1", $path) };
	die "failed to get full commit SHA for $path: $out" if $err;
	die "rev doesn't looks like a commit SHA: $out" unless ($out =~ m/^${short_sha}[a-z0-9]+/);
	return $out;
}

# specfile_update_version updates the version string and also the changelog
# of the specified module.
sub specfile_update_version($module) {
	return unless $module->{specfile};

	open(my $fh_in, '<', $module->{specfile}) or
		die "failed to open $module->{specfile}: $!.";
	open(my $fh_out, '>', $module->{specfile} . '.new') or 
		die "failed to open $module->{specfile}: $!.";

	my $changelog_section = 0;
	my $changelog_text = specfile_prepare_changelog($module);
	
	say "Updating specfile for $module->{name} ...";

	my $has_commit = 0;
	my $matches = 0;

	my $version_prev = $module->{version_prev};
	my $version_curr = $module->{version_curr};
	my ($commit_prev, $commit_curr);

	# some modules carry both commit and version numbers at the same
	# time. also, some versions don't change, but commit ref do.
	unless ($module->{version_curr} =~ /^[\.0-9]+$/) {
		$has_commit = 1;
		$version_prev = $module->{version_prev} =~ s/^([\.0-9]+)-.*/$1/r;
		$version_curr = $module->{version_curr} =~ s/^([\.0-9]+)-.*/$1/r;
		$commit_prev = $module->{version_prev} =~ s/^.*-([a-zA-Z0-9]+)$/$1/r;
		$commit_curr = $module->{version_curr} =~ s/^.*-([a-zA-Z0-9]+)$/$1/r;

		$commit_prev = commit_full_sha("$DEFAULT_CACHE_DIR/$module->{name}",
			$commit_prev);
		$commit_curr = commit_full_sha("$DEFAULT_CACHE_DIR/$module->{name}",
			$commit_curr);
	}

	while (<$fh_in>) {
		if ($has_commit) {
			$matches++ if (s/^(Version:\s+)$version_prev$/$1$version_curr/);
			$matches++ if (s/^(%global commit\s+)$commit_prev.*/$1$commit_curr/);
		} else {
			$matches++ if (s/^(Version:\s+)$module->{version_prev}$/$1$module->{version_curr}/);
		}

		if ($changelog_section) {
			print $fh_out $changelog_text;
			$changelog_section = 0;
		}
		$changelog_section = 1 if (/%changelog/);

		print $fh_out $_;
	}

	# mod has update, but no 'Version:' or '%global commit' matched.
	if ($matches == 0) {
		say "WARNING: No 'Version:' or '%global commit' was updated. Please, check it manually";
	}

	close $fh_in;
	close $fh_out;
	move($module->{specfile}.'.new', $module->{specfile});
	return;
}

# specfile_update_bundled is similiar to specfile_update(), but it update the
# bundled modules version instead, which are passed as "Provides:" to a parent
# module.
sub specfile_update_bundled($module, $bundled) {
	say "Updating bundled module $bundled->{name} in $module->{name} ...";

	open(my $fh_in, '<', $module->{specfile}) or
		die "failed to open $module->{specfile}: $!.";
	open(my $fh_out, '>', $module->{specfile} . '.new') or
		die "failed to open $module->{specfile}: $!.";

	my $match = 0;
	my $section_start = 0;
	my $is_new = $bundled->{version_prev} ? 0 : 1;

	while (<$fh_in>) {
		# if a module doesn't have version_prev set, but is in this place of
		# the code, it means it's a new module being provided as bundled.
		if ($is_new) {
			$match = m/^Provides:.*/mg;
			# mark the beginning of Provides section
			$section_start = 1 if $match;
		} else {
			$match = s/^(Provides:\s+bundled\(golang\($bundled->{url}\)\) = )$bundled->{version_prev}/$1$bundled->{version_curr}/mg;
		}

		# add new provides at the end of Provides section
		if (not $match and $section_start and $is_new) {
			my $new_provides = "Provides:\tbundled(golang($bundled->{url})) = $bundled->{version_curr}\n";
			print $fh_out $new_provides;
			$section_start = 0;
		}

		print $fh_out $_;
	}

	close $fh_in;
	close $fh_out;
	move($module->{specfile}.'.new', $module->{specfile});
	return;
}

# specfile_update_buildrequires is similiar to specfile_update(), but it update the
# bundled modules version instead, which are passed as "Provides:" to a parent
# module.
sub specfile_update_buildrequires($module, $req) {
	say "Updating bundled module $req->{name} in $module->{name} ...";

	open(my $fh_in, '<', $module->{specfile}) or
		die "failed to open $module->{specfile}: $!.";
	open(my $fh_out, '>', $module->{specfile} . '.new') or
		die "failed to open $module->{specfile}: $!.";

	my $match = 0;
	my $section_start = 0;
	my $is_new = $req->{version_prev} ? 0 : 1;

	while (<$fh_in>) {
		# if a module doesn't have version_prev set and already reached this
		# far into the code, it means it's a new module required for building
		if ($is_new) {
			$match = m/^BuildRequires:.*/mg;
			# mark the beginning of BuildRequires section
			$section_start = 1 if $match;
		} else {
			$match = s/^(BuildRequires:\s+golang\($req->{url}\) = )$req->{version_prev}/$1$req->{version_curr}/mg;
		}

		# add the new build requirement at the end of BuildRequires section
		if (not $match and $section_start and $is_new) {
			my $new_buildreq = "BuildRequires:\tgolang($req->{url}) = $req->{version_curr}\n";
			print $fh_out $new_buildreq;
			$section_start = 0;
		}

		print $fh_out $_;
	}

	close $fh_in;
	close $fh_out;
	move($module->{specfile}.'.new', $module->{specfile});
	return;
}

# specfile_create relies on go2rpm application to create specfiles for those
# go modules that we still don't have a specfile for.
# Return:
# 	An %go_module_info reference
sub specfile_create($module, $dir) {
	my %mod_tmp = %$module;
	my $go_to_rpm_exec = 'go2rpm';

	if (not which($go_to_rpm_exec)) {
		warn "$go_to_rpm_exec not find. Skipping specfile creation";
		return;
	}

	if (not -e $dir) {
		warn "$dir not find. Skipping specfile creation";
		return;
	}

	say "Creating specfile for $module->{name} ...";
	# if the version string of a module is a commit SHA we should use -c
	# instead of -v
	my $exec_version_opt = ($mod_tmp{version_curr} =~ /^[\.0-9]+$/) ? '-v' : '-c';
	my $cmdline = qq{
		$go_to_rpm_exec \
		--no-auto-changelog-entry --stdout \
		$exec_version_opt $mod_tmp{version_curr} \
		$mod_tmp{url} 2>&1 > $mod_tmp{name}.spec
	};
	$cmdline =~ s/\n//g;
	(undef, my $err) = @{ run_command($cmdline, $dir) };
	die "failed to convert go to rpm file" if $err;

	$mod_tmp{specfile} = "$dir/$module->{name}.spec";

	return \%mod_tmp;
}

# module_url_sanitizer check the module URL for certain cases where it must
# be changed before passed to the specfile creation tool, otherwise it'll
# fail. The cases are not complete and may grow as the number of dependencies
# increase.
# Return:
#   The new URL string or empty in case the module should be skipped.
sub module_url_sanitizer($mod_url) {
	return "" if (not $mod_url);
	return "" if ($mod_url =~ m/^[^\w\s]+$/);
	# skip golang version
	return "" if ($mod_url =~ m/^[\d.]+$/);
	# skip golang official modules
	return "" if ($mod_url =~ m/^golang.org\/.*/);
	# remove trailing API version number, i.e. .*/v2
	$mod_url =~ s/\/v[0-9]+$//;
	return $mod_url;
}

# lab_modules parses the git diff from the last lab version to the current
# one wrt the go.mod file, where the dependency modules and their versions
# are stored. In this function the lab itself is also put as one of the
# modules to be updated.
# Return:
# 	An array of %go_module_info references
sub lab_modules($repo_path) {
	my @mods_temp;

	say "Checking what dependencies were updated...";

	# get the last two versions of lab (the current and previous)
	my ($tag_curr, $tag_prev) = @{ git_tag_diff($repo_path) };

	# diff go.mod between the two versions
	my ($diff, $err) = @{ run_command("git diff --unified=0 $tag_prev..$tag_curr -- go.mod",
										$repo_path) };
	die "failed to get git diff:\n$diff" if $err;

	foreach my $line (split('\n', $diff)) {
		# ignore lines that we aren't interested
		next if (index($line, 'go.mod') >= 0);
		next if (
			(not substr($line, 0, 1) eq '+') and
			(not substr($line, 0, 1) eq '-')
		);

		my @gomod_info = split('\s', $line);
		# sanitize URL for possible skips
		my $mod_url = module_url_sanitizer($gomod_info[1]);
		next if (not $mod_url);

		my %dep_mod_info = %go_module_info;
		$dep_mod_info{url} = $mod_url;
		# translate the module name to the way found in Fedora/RHEL repos
		my $mod_name = 'golang-' . $mod_url;
		$mod_name =~ s/go-//;
		$mod_name =~ tr/\//-/;
		$mod_name =~ s/\.com//;
		$dep_mod_info{name} = $mod_name;

		# check if we already have the module placed in the list by comparing
		# its name		
		my $mod_found = 0;
		my $mod_idx = 0;
		while (my ($idx, $mod) = each @mods_temp) {
			if ($mod->{name} eq $mod_name) {
				%dep_mod_info = %$mod;
				$mod_idx = $idx;
				$mod_found = 1;
				last;
			}
		}

		# save current and previous version without the leading 'v', since
		# it'll be used when checking the RPM specfiles, which doesn't contain
		# the lead letter.
		my $mod_version = $gomod_info[2];
		$mod_version =~ s/v//;
		if (substr($line, 0, 1) eq '+') {
			$dep_mod_info{version_curr} = $mod_version;
		} else {
			$dep_mod_info{version_prev} = $mod_version;
		}
		
		# update an existing module or create a new entry?
		if ($mod_found) {
			$mods_temp[$mod_idx] = \%dep_mod_info;
		} else {
			push @mods_temp, \%dep_mod_info;
		}
	}

	# only include those modules that were actually updated or are new,
	# modules without 'version_curr' means they're being removed completely
	# from the dependency list, so we should not care about them.
	my @modules;
	foreach (@mods_temp) {
		push @modules, $_ if (defined $_->{version_curr});
	}

	# lab itself isn't module, but needs to be considered as one for getting
	# its own specfile updated.
	my $ver_curr = $tag_curr =~ s/v//r;
	my $ver_prev = $tag_prev =~ s/v//r;

	my %lab_mod_info = %go_module_info;
	$lab_mod_info{name} = 'lab';
	$lab_mod_info{url} = $DEFAULT_LAB_REPO_URL;
	$lab_mod_info{version_curr} = $ver_curr;
	$lab_mod_info{version_prev} = $ver_prev;
	push @modules, \%lab_mod_info;

	return \@modules;
}

# lab_map_module_specfile map the specfiles in $path to each module.
# This function adds a field in the module info hash, but does so in a copy
# preserving the contents of the input $modules.
# Return:
# 	An array of %go_module_info references
sub lab_map_module_specfile($modules, $dir) {
	my $extension = '.spec';
	my @specfiles = glob($dir. '*' . $extension);
	# we are going to modify the array items in this function. 
	# perform a deep copy of the items, lets avoid side-effects.
	my $mods_temp = [ map +{ %$_ }, @$modules ];

	# map module to the specfile name if present in $path.
	# this step adds the field "specfile = <string>" in the module info hash
	foreach my $spec (@specfiles) {
		foreach my $mod (@$mods_temp) {
			if ($mod->{name} eq substr($spec, length $dir, -(length $extension))) {
				$mod->{specfile} = $spec;
				last;
			}
		}
	}

	return $mods_temp;
}

# lab_repo_check searches for the module name in the fedora repository,
# if something is found, we can left it out from the modules being tracked,
# since it won't need a specific specfile.
# Return:
# 	An array of %go_module_info references
sub lab_repo_check($modules) {
	return $modules if $opts{skip_repo_check};

	say 'Checking for modules in the official repos...';

	my @norepo_mods;

	foreach my $mod (@$modules) {
		# We don't check lab in the repos, for obvious reasons
		if ($mod->{name} eq 'lab') {
			push @norepo_mods, $mod;
			next;
		}

		# disable the our own COPR repo during yum lookup to avoid wrongly
		# finding all packages in the "official repos".
		my $yum_cmd = "yum --disablerepo copr:copr.fedorainfracloud.org:bmeneguele:rhkernel-devtools";

		# use 'yum repoquery' instead of 'yum search' (RHBZ#2027412)
		my ($out, $err) = @{ run_command("$yum_cmd repoquery $mod->{name}-devel 2>&1", '.') };
		die "failed to query for package $mod->{name}" if $err;
		if ($out =~ /$mod->{name}/) {
			if ($mod->{specfile}) {
				say "NOTE: $mod->{name} seems to be present in the official repos";
				# even if the modules seems to be in the official repo, lets
				# continue using the physical specfiles until someone
				# deletes them.
				push @norepo_mods, $mod;
			}
		} else {
			push @norepo_mods, $mod;
		}
	}

	return \@norepo_mods;
}

# lab_update_fedora_specfile updates Fedora module's specfile only.
# Since on Fedora we explicitly build lab, we have each dependency, not found
# in Fedora's official repo, as a separate specfile, which we also need to
# build ourselves.
sub lab_update_fedora_specfile($modules, $dir) {
	say "Updating modules for Fedora...";

	my $mods_info = lab_repo_check($modules);

	# get specific lab module
	my @lab_mods = grep { $_->{name} eq 'lab' } @$mods_info;
	die "more than one 'lab' module was found" if (scalar @lab_mods > 1);
	my $lab_mod = $lab_mods[0];
	
	foreach my $mod (@$mods_info) {
		say "==> $mod->{name}";
		git_clone($mod->{url}, "$DEFAULT_CACHE_DIR/$mod->{name}");
		git_update("$DEFAULT_CACHE_DIR/$mod->{name}");
		specfile_update_version($mod);

		next if ($mod->{name} eq 'lab');
		$mod = specfile_create($mod, $dir) unless $mod->{specfile};
		specfile_update_buildrequires($lab_mod, $mod);
	}

	return;
}

# lab_update_rhel_specfile updates the RHEL specfile only.
# This is somewhat special: we directly download the lab executable here
# instead of installing each dependency and then building the lab executable.
# With that, we declare the dependency as "bundled" to the package.
sub lab_update_rhel_specfile($modules) {
	say "Updating modules for RHEL...";

	my $lab_mod;

	foreach my $mod (@$modules) {
		next if (not $mod->{name} eq 'lab');
		specfile_update_version($mod);
		$lab_mod = $mod;
		last;
	}

	foreach my $mod (@$modules) {
		next if ($mod->{name} eq 'lab');
		specfile_update_bundled($lab_mod, $mod);
	}

	return;
}

# git_clone clone a certain git repo url to the specified path.
sub git_clone($url, $path) {
	if (-e $path) {
		say "$url already cloned at $path";
		return;
	}
	say "Cloning $url repository into $path ...";
	# use HTTPS for cloning. we should use this path as read-only.
	my ($out, $err) = @{ run_command("git clone https://$url $path 2>&1", '.') };
	die "failed to clone repository $url: $out" if $err;
}

# git_update pulls all new refs and tags from remote repo.
sub git_update($path) {
	# fetch any new tags in the upstream repo
	say 'Updating repository ...';
	my ($out, $err) = @{ run_command('git pull --tags --prune 2>&1', "$path") };
	die "failed to update repository at $path" if $err;
}

sub main {
	args_parse();

	# set the default values
	my $lab_path = $opts{repo_path} // $DEFAULT_LAB_REPO_PATH;
	my $lab_url = $opts{repo_url} // $DEFAULT_LAB_REPO_URL;
	my $specs_dir_rhel = $opts{specs_dir_rhel} // $DEFAULT_SPECS_DIR_RHEL;
	my $specs_dir_fedora = $opts{specs_dir_fedora} // $DEFAULT_SPECS_DIR_FEDORA;

	# make sure we have our repo dir in place
	git_clone($lab_url, $lab_path);
	say "Using repository path \"$lab_path\"";
	git_update($lab_path);

	# Get all updated modules (lab included)
	my $modules = lab_modules($lab_path);

	# Fedora specfiles update.
	# Get the list of dependency modules.
	if (not $opts{skip_fedora}) {
		my $modules_fedora = lab_map_module_specfile($modules,
			$specs_dir_fedora);
		lab_update_fedora_specfile($modules_fedora, $specs_dir_fedora);
	}

	# RHEL specfile update: we _bundle_ module dependencies for RHEL and
	# because of that we need a separate step for its specfile.
	if (not $opts{skip_rhel}) {
		my $modules_rhel = lab_map_module_specfile($modules,
			$specs_dir_rhel);
		lab_update_rhel_specfile($modules_rhel);
	}

	say 'DONE';
	return 0;
}


__END__

=head1 NAME

Lab tool auto-updater

=head1 DESCRIPTION

This program will try to check what modules were updated, added or removed
from the previous lab version to the newer one and try to automatically
manage the respective specfiles (Fedora and RHEL).

=head1 SYNOPSIS

./auto-updater.pl [options]

=head2 Options summary

  --changelog                Edit changelog at runtime
  --packager <info>          Packager "name <email>" information
  --repo-path <path>         Custom lab local git tree path (default: .cache/lab)
  --repo-url <url>           Custom lab git repository URL (default: upstream)
  --range <v..v>             Previous and new lab version number
  --specs-dir-rhel <dir>     RHEL RPM spec files directory (default: ./specs/rhel)
  --specs-dir-fedora <dir>   Fedora RPM spec files directory (default: ./specs/fedora)
  --skip-fedora              Skip Fedora spec file updates
  --skip-rhel                Skip RHEL spec file updates
  --skip-repo-check          Skip checking modules on official repositories
  --help                     This help message
  --man                      A more compreensive help text

=head1 OPTIONS

=over 4

=item B<--changelog>

This option open the system's editor for editing the specfile changelog
text, which, by default, is automatically populated with the string

	"Lab release update, v<number>"

The system editor is the one specified in the $EDITOR environment variable.

=item B<--packager=<info>>

The packager "name <email>" to be used in the specfile's changelog section.
An <info> like "Joe Packager <jpkg@domain.com>" results in a header like the
following:

* Tue May 25 2021 Joe Packager <jpkg@domain.com> - 0.1.2-1

=item B<--lab-path=<path>>

This option allow the user to specify the path to a local lab git tree. It's
expected the tree is up-to-date with upstream repository and all tags
pulled. This is used for diff'ing the previous and current version of
go.mod file. If nothing is passed, the path ./.cache is used as the folder
to clone/pull the upstream tree.

=item B<--lab-url=<url>>

Instead of using the default upstream repository URL for cloning the project,
use this custom URL. This is helpful for debugging purposes, considering the
user can set the URL to his own upstream fork.

=item B<--range=<version>..<version>>

This option allow the user to specify from which version number to which one
the lab tool is being updated to. If nothing is passed, this tool will fetch
the last two tags from the upstream repository and consider them to be the 
<latest> and <latest-1>, in other words, the newest tag will be considered
the version to which the specfiles should be updated to, while the oldest tag
will be considered the previous version from which the specfile is being
updated from.

=item B<--specs-dir-rhel=<dir>>

Directory where all RPM spec files for RHEL are (or should be) placed.

=item B<--specs-dir-fedora=<dir>>

Directory where all RPM spec files for Fedora are (or should be) placed.

=item B<--skip-fedora>

Skip any spec file update for Fedora.

=item B<--skip-rhel>

Skip any spec file update for RHEL.

=item B<--skip-repo-check>

Skip the step of checking if the modules are present in the official
repositories.

=item B<--help>

Print the brief help message, containing only the SYNOPSIS information.

=item B<--man>

Print this manual page.

=back

=cut

%ifarch x86_32
%global pkg_name lab_%{version}_linux_386.tar.gz
%endif
%ifarch x86_64
%global pkg_name lab_%{version}_linux_amd64.tar.gz
%endif

# Distro completion files dir
%define completion_dir %(pkg-config --variable=completionsdir bash-completion)
%if "%{completion_dir}" == ""
%define completion_dir /etc/bash_completion.d/
%endif

%global gourl	https://github.com/zaquestion/lab

Name:		lab
Version:	0.25.1
Release:	1%{?dist}
Summary:	GitLab cli tool
License:	CC0
URL:		%{gourl}
Source:		%{gourl}/releases/download/v%{version}/%{pkg_name}
Source1:	lab.bash_completion

Requires:	git

Provides:	%{_bindir}/%{name}
Provides:	bundled(golang(github.com/araddon/dateparse)) = d820a6159ab1e961778690fea995231df707ba0d
Provides:	bundled(golang(github.com/avast/retry-go)) = 1.0.1
Provides:	bundled(golang(github.com/charmbracelet/glamour)) = 0.3.0
Provides:	bundled(golang(github.com/go-git/go-billy)) = 5.0.0
Provides:	bundled(golang(github.com/rivo/tview)) = 82b05c9fb32936cffce7a03c24d891b43a79b82e
Provides:	bundled(golang(github.com/rsteube/carapace)) = 0.4.1
Provides:	bundled(golang(github.com/tcnksm/go-gitconfig)) = 0.1.2
Provides:	bundled(golang(github.com/xanzy/go-gitlab)) = 0.43.0
Provides:	bundled(golang(github.com/otiai10/copy)) = 1.6.0
Provides:	bundled(golang(github.com/MakeNowJust/heredoc)) = 2.0.1
Provides:	bundled(golang(github.com/ProtonMail/go-crypto)) = 428f8eabeeb3a9b3fe1d2485cc516efeddcc3358
Provides:	bundled(golang(github.com/cpuguy83/go-md2man)) = 2.0.1
Provides:	bundled(golang(github.com/google/go-querystring)) = 1.1.0
Provides:	bundled(golang(github.com/hashicorp/go-cleanhttp)) = 0.5.2
Provides:	bundled(golang(github.com/sergi/go-diff)) = 1.2.0
Provides:	bundled(golang(github.com/whilp/git-urls)) = 1.0.0

%description
Lab wraps Git or Hub, making it simple to clone, fork, and interact with
repositories on GitLab.

%prep
%setup -q -c -n %{name}-%{version}

%install
install -m 0755 -vd %{buildroot}%{_bindir}
install -m 0755 -vp %{_builddir}/%{name}-%{version}/lab %{buildroot}%{_bindir}/
install -m 0755 -vd %{buildroot}%{completion_dir}
install -m 0644 -vp %{SOURCE1} %{buildroot}%{completion_dir}/lab

%files
%license LICENSE
%doc README.md
%{_bindir}/*
%{completion_dir}

%changelog
* Tue Jul 05 2022 Bruno Meneguele <bmeneg@redhat.com> - 0.25.1-1
- Rebase to upstream release 0.25.1

* Wed Mar 23 2022 Bruno Meneguele <bmeneg@redhat.com> - 0.24.0-1
- Rebase to upstream release 0.24.0

* Mon Dec 06 2021 Bruno Meneguele <bmeneg@redhat.com> - 0.23.0-1
- Rebase to upstream release 0.23.0

* Tue Jun 01 2021 Bruno Meneguele <bmeneg@redhat.com> - 0.22.0-1
- Rebase to upstream release 0.22.0

* Tue Mar 16 2021 Bruno Meneguele <bmeneg@redhat.com> - 0.21.0-1
- Rebase to the latest upstream release

* Wed Feb 24 2021 Bruno Meneguele <bmeneg@redhat.com> - 0.20.0-1
- Rebase to the latest upstream release
- Add bash completion file

* Mon Jan 25 2021 Bruno Meneguele <bmeneg@redhat.com> - 0.19.0-1
- Rebase to the latest official release

* Thu Jan 14 2021 Bruno Meneguele <bmeneg@redhat.com> - 0.18.0-1
- Rebase to the latest official release

* Thu Nov 05 2020 Bruno Meneguele <bmeneg@redhat.com> - 0.17.2-3
- Add "git" as a runtime requirement
- Also add the executable as "provides"

* Wed Nov 04 2020 Bruno Meneguele <bmeneg@redhat.com> - 0.17.2-2
- Add "provides:" with bundled info

* Tue Oct 13 2020 Bruno Meneguele <bmeneg@redhat.com> - 0.17.2-1
- Initial package

# golang-github-protonmail-crypto.spec
# Generated by go2rpm 1.5.0
%bcond_without check

# https://github.com/ProtonMail/go-crypto
%global goipath         github.com/ProtonMail/go-crypto
%global commit          428f8eabeeb3a9b3fe1d2485cc516efeddcc3358

%gometa

%global common_description %{expand:
[mirror] Go supplementary cryptography libraries.}

%global golicenses      LICENSE PATENTS
%global godocs          README.md AUTHORS CONTRIBUTORS

Name:           %{goname}
Version:        0
Release:        0.1%{?dist}
Summary:        [mirror] Go supplementary cryptography libraries

# Upstream license specification: BSD-3-Clause
License:        BSD
URL:            %{gourl}
Source0:        %{gosource}

BuildRequires:  golang(golang.org/x/crypto/cast5)
BuildRequires:  golang(golang.org/x/crypto/curve25519)
BuildRequires:  golang(golang.org/x/crypto/ed25519)

%if %{with check}
# Tests
BuildRequires:  golang(golang.org/x/crypto/ripemd160)
%endif

%description
%{common_description}

%gopkg

%prep
%goprep

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%gopkgfiles

%changelog
* Mon Dec 06 2021 Bruno Meneguele <bmeneg@redhat.com> - 0-0.1
- Initial commit



# Generated by go2rpm 1.2
%bcond_without check

# https://github.com/alecthomas/go-thrift
%global goipath         github.com/alecthomas/go-thrift
%global commit          636b2a6efaf66d143be2e307fa9d8f0619e5b103

%gometa

%global common_description %{expand:
A native Thrift package for Go.}

%global golicenses      LICENSE
%global godocs          examples README.md

Name:           %{goname}
Version:        0
Release:        1%{?dist}
Summary:        A native Thrift package for Go

# Upstream license specification: BSD-3-Clause
License:        BSD
URL:            %{gourl}
Source0:        %{gosource}

%description
%{common_description}

%gopkg

%prep
%goprep

%build
for cmd in cmd/* ; do
  %gobuild -o %{gobuilddir}/bin/$(basename $cmd) %{goipath}/$cmd
done

%install
%gopkginstall
install -m 0755 -vd                     %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/* %{buildroot}%{_bindir}/

%if %{with check}
%check
%gocheck
%endif

%files
%license LICENSE
%doc examples README.md
%{_bindir}/*

%gopkgfiles

%changelog
* Tue Oct 06 14:44:48 CEST 2020 Patrick Talbert <ptalbert@redhat.com> - 0-1.git636b2a6
- Initial package

# Red Hat Kernel Developer Tools

This repository keep some files (mostly RPM specfiles) and other information
for the tools that are available at [bmeneguele/rhkernel-devtools COPR
repo](https://copr.fedorainfracloud.org/coprs/bmeneguele/rhkernel-devtools/).

# Updating steps

Whenever lab upstream releases a new version we need to track which Go modules were
updated in lab's `go.mod` file and update their respective RPM specfiles here.
This work can be somewhat tedious when the number of modules being updated for lab
is greater then a certain $limit. Because of that we have the `auto-updater.pl`
script in place.

It's written in Perl and requires some out-of-core dependencies, but all of them are
at MetaCPAN and `cpanm` can be used to install them.

First, install `cpanm` if you don't have it:
```
$ curl -L https://cpanmin.us | perl - App::cpanminus
# OR
# dnf install perl-App-cpanminus
```

Then install the required dependencies:
```
$ cpanm --installdeps .
```

With the environment setup, make sure to check the script options:
```
$ ./auto-updater.pl --help
```

For a single update run and no interactivity at all, you can use something like:
```
$ ./auto-updater.pl --range v0.21.0..v0.22.0 --packager "Joe Packager <jpkger@domain.com>"
```

Which will update the specfiles from considering `v0.21.0` as the previous lab version and
`v0.22.0` as the new one to which the specfiles should be updated to.

For more examples and detailed information about the scripts, please check:
```
$ ./auto-updater.pl --man
```

# Bugs

In case you find any packaging related bug, please feel free to open up
issues at the
[issues](https://gitlab.com/ptalbert/rhkernel-devtools/-/issues) menu.
